# Convert *Hz* to *Cents*

To finalize, the result, which is expressed in **ΔHz** units, is converted to [**cents**](https://en.wikipedia.org/wiki/Cent_(music)) - a logarithmic unit of measure, which is much more convenient for tuning, because it gives equal gradation for every note.

***All is done!*** Now it is good to go with inputing the result to any UI implementation.
