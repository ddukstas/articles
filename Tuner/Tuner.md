# Guide to Guitar Tuner in a Browser

[WebAudioAPI](https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API) is powerful enough to build a guitar tuner in a browser. In this guide I'll walk through all the necessary steps (including programming, physics and music theory) to do that. 

1. [Get a frequency spectrum](./1.GetFreqSpectrum.md)
2. [Find fundamental peak freq. (in 50-400Hz range) in a spectrum](./2.FindFundamentalFreq.md)
3. [Find harmonic peaks frequencies in a spectrum](./3.FindHarmonics.md)
4. [Calculate the harmonic average distance to nearest in-tune note](./4.DetermineOutOfTuneLevel.md)
5. [Convert the diff to cents and represent it](./5.ConvertHzToCents.md)


## References
* http://amath.colorado.edu/pub/matlab/music/MathMusic.pdf
* http://www.tedknowlton.com/resume/CCPPT.htm
* https://jakevdp.github.io/blog/2013/08/28/understanding-the-fft/
* [./misc/GuitarFretboardFrequencies.md](./misc/GuitarFretboardFrequencies.md)
