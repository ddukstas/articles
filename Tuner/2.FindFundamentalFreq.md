# Finding Fundamental Frequency

There are two approaches:
1. Finding the first *zero slope* location. 
2. Finding the location of the maximum value.

While the 2nd method is easier to implement, the 1st method works better in cases where the **fundamental** has lower volume than the **overtones** because of non-linear microphone frequency response.

## Interpolation

After finding the assumed fundamental freq. using one of the methods described above, there is still an oportunity to increase the accuracy of a result by slightly shifting it. This can be achieved using interpolation.

It is a process of finding an actual peak which is usually lost between the two indexes because of limited sampling resolution. E.g.: 
```
   x
  x|  
  ||x
xx|||xx
0123456
```
In the example the max. value is `i=3`. However, the only continuous curve which would overlap the sampled points, would have a peak between `i=2` and `i=3`. (because math lol)

The algorithm to find the actual peak point is to calculate a weighted average of few points around the assumed peak value using signal strength as a peak factor. In our example that would be:
```javascript
actualPeakIndex = (2*d[2] + 3*d[3] + 4*d[4]) / (d[2] + d[3] + d[4]);
// `d` is an array of the sample.
``` 

[Continue →](3.FindHarmonics.md)