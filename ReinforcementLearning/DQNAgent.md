# Reinforcement learning guide

The tetris bot is build on a few classical reinforcement learning ideas - [**Markov decision process**](https://en.wikipedia.org/wiki/Markov_decision_process), [**Bellman equation**](https://en.wikipedia.org/wiki/Bellman_equation) and [**artificial neural networks**](https://en.wikipedia.org/wiki/Artificial_neural_network). The combination of these ideas is a foundation of [**deep Q learning**](https://en.wikipedia.org/wiki/Q-learning#Deep_Q-learning) (one type of [reinforcement learning algorithms](https://en.wikipedia.org/wiki/Reinforcement_learning#Comparison_of_reinforcement_learning_algorithms)) used here.

Althought deep q-learning algorithms are generic, practically the implementations of the bots are always more or less coupled to the environment. Thus, transfer to the new environments (e.g. other games) requires some manual work and creativity. 

## Defining bot's environment

The bot can be thought as a function having some inputs and output. There are usually two inputs: the state of the environment and the reward (or punishment) for the action the bot has taken. This action would be the sole output of that function.

This function is usually through some time interval or hooked to be triggered on some game events. In this particular implementation it is triggered every time the new brick spawns. 

### Inputs (state and reward)
- **the state of the environment** - typically pixels of the game screen (what human player sees, just downsampled). It is used to make a decision and to train the bot.
- **reward** - typically a numeric value representing how good preceding action was. Negative value - is a punishment. It is used to train the bot.

### Output (bot's taken action)

It is an action the bot has decided to take. The decision is calculated from the given environment state.

### Note on implementation

The environment is usually implemented manually during the bot's development process, unless the game's implementation provides sufficient inputs/outputs out-of-the-box.

### Wrapping up

Based the game's state the bot makes a decision to take an action, then it observes a new state and repeats the whole process again. This is called a **Markov's decision processing**. The diagram to wrap up:

```
... -> [state, reward] -> decision -> action -> [state, reward] -> decision -> ...
```

## Decision making

Lets stop at the point where the bot encounters a new state. How does it go from having a state object to making a choice, exactly?

### Choosing the algorithm

There are a few ways to implement a decision making. The obvious solution is to **pre-program it manually**, but cool kids these days use **machine learning algorithms** to make this part more environment independent. There are numerous choices for that. 

**The simplest being "Q table learing"**, but it is suitable only for very very small environments (like 4x4 grid and 4 actions), because increasing environment size exponentially increase memory hardware requirements. 

**The most popular (arguably) is Deep Q Learning algorithm**, popularized by Google's paper on unsupervised learning of Atari video games. I decided to go with this one as I didn't explore other options yet.

### Reinforcement learning

The underlying idea is simple and very well known in behavioral psychology: when the subject has done a desired action reward it, otherwise - punish it and hope it will eventually remember the associations:

> Good cat, take a snack! Bad catto, schhh!
> 
> ![Animation of cats ringing a bell for a snack](./PavlovCat.gif)

In case of our bot, it has to be implemented in such a way that every time it does the action and receives a reward (or punishment) it associates the two together and memorize it. That's done with built-in neural net.

> Good bot, you filled 2 rows, take a reward=10! Bad botto, you grew a pile, reward=-5!

### Taking the future into account

Going even further, after taking some action the bot ideally would associate it not only with the immediate reward, but also with the rewards that will happen in the future. Formally, it is described using the **Bellman equation**: 

> Q[t][takenAction] = reward +  σ * max(Q[t+1])

***Q* is a shorthand for quality**. That's where the mystical letter Q in the "Deep Q Learning" title comes from!

**σ** - future discount rate. Has 0 to 1 value and describes how far into the future the current action quality is considered.

The *Q* value is calculated with the Bellman equation for each state and then is used to train the **Deep Q network**.

### The neural network design

The **deep Q network model** is designed to take in the environment state as an input and make a prediction of qualities for each possible action in that state:

```
QOfEachAction = model.predict(state)
bestAction = argmax(QOfEachAction)
```
(Then the bestAction can be taken, e.g.: `doAction(bestAction)`)

The model can be trained after each taken action:
```
[newState, reward] = doAction(bestAction)

QOfEachFutureAction = model.predict(newState)
Q[t][bestAction] = reward + σ * max(QOfEachFutureAction)
model.fit(state, Q[t])
```

However, training the model after each action is inefficient, thus in practice the state-reward pairs are stored into **replay memory** and are trained in batches (e.g. every n actions or after each round).

Having the **replay memory** gives one more advantage - the network can be trained in shuffled batches to avoid overfitting the network on adjacent states which are very similar in a lot of environments.

#### Double Q-learning

If you were attentive, you might have noticed the above deep q-learning algorithm is chasing its own tail. For this reason the improvement called [double q-learning](https://en.wikipedia.org/wiki/Q-learning#Double_Q-learning) can increase the performance.
 

### Exploration vs exploitation

Sometimes random action needs to be taken to explore the new ways to act. The most popular implementation (I guess because its the most simple one) is the **epsillon (*ε*) greedy strategy**. Basically, *ε* is a variable describing the probability for a bot to take random action at a given state and it is being gradually decreased after each learning cycle, thus the bot do less exploration as it learns.

There are other exploration strategies that I will explore in the future.

## Summary

To wrap up, here's a step-by-step algorithm of a **deep Q network** bot:
```
1. init replay memory & networks
2. Play a match. For each step:
    1. action = argmax(predict_Q(state))
    2. next_state, reward = do_action(action)
    3. memory.append(state, action, reward, next_state)
3. Pick random (to decrease in-between-correlation) memory sample
4. Train the network with that sample:
    1. state, action, reward, next_state = sample.pop()
    2. max_next_q = max(predict_Q(next_state))
    3. new_q = reward + DISCOUNT * max_next_q (Bellman eq.)
    4. model.fit(state, new_q)

5. Repeat with new match until trained.

```