# Tech Articles

There are plenty of articles on things I am working on, thus I tend to write only on topics that I find challenging. The process helps me to consolidate my own understanding and complex topics is where additional point of view is the most beneficial for a community. Here is some hopefully useful information collected from my experience:

* [P2P in a Browser](./WebRTC/WebRTC.md) - a short guide to establish a WebRTC connection
* [DQN Agent for Tetris](./ReinforcementLearning/DQNAgent.md) - a guide to develop and train a tetris playing agent
* [Tuner in a browser](./Tuner/Tuner.md) - a guide to implement a guitar tuner in browser

My homepage - [dukstas.lt](https://dukstas.lt).
