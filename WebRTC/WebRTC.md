# WebRTC guide

WebRTC is a P2P communication protocol used in a browser environment. It is supported in all major browsers.
I found the [official documentation](https://webrtc.org/getting-started/peer-connections) confusing and [MDN docs](https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API/Connectivity) are still in draft stage, so I decided to create a short guide how to setup a WebRTC connection for a web application.

For a live demo go checkout my online tetris project: https://dukstas.lt/tetris.

## Establishing a connection

WebRTC p2p connection between two browsers is established via the process called **signaling**. 

### ICE candidates

This process consists of **ICE candidates** exchange between the two browsers. First both browsers request an **ICE candidate** from one of many public **ICE servers**, then they exchange it with each other via our own http or web socket servers (see my thoughts about this on the bottom **\***). **ICE candidates** are basically objects containing connectivity information (e.g. protocol, port, IP, etc.). See more details [here](https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API/Connectivity#ICE_candidates).

Here's example ICE candidate object:
![ICE candidate object](./ICECandidate.png)

### Offers and answers

Two more messages exchanged between the browsers are RTC session descriptions (offer-answer pair) which contain description of media transfer endpoint (e.g. kind of media being sent, protocol, IP, port, etc.). See more details [here](https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API/Connectivity).

Here's example RTC session description object:
![RTC session description object](./RTCDescription.png)

### The algorithm

Although the roles of the browsers on both ends are identical, the algorithm to establish connection isn't identical. For this reason I'll label one browser as a **host**, another - **guest**. Here is a step-by-step overview what actions have to be taken in both browsers using [RTCPeerConnection API](https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection) to establish a WebRTC P2P connection:

1. Prepare ICE candidate listeners and handlers:
    - **Both browsers** initialize the API, e.g. `const c = new RTCPeerConnection(iceServers: ["http://...", ...]);`
    - **Both browsers** listen for ICE candidates incoming from another browser via http or web socket server and register once received (e.g. `c.connection.addIceCandidate(candidateFromAnotherBrowser)`)
    - **Both browsers** listen for incoming ICE candidates from ICE servers and send it to the other browser once received (e.g. `c.connection.addEventListener('icecandidate', (e: RTCPeerConnectionIceEvent) => ws.broadcast(e.candidate)`)

2. Establish peer connection:
    - **Guest** creates an offer (e.g. `offer = await c.createOffer()`) 
    - **Guest** sets it as a *local* description (e.g. `c.setLocalDescription(offer)`)
    - **Guest** sends it to the host **\***
    - **Host** sets received offer as a *remote* description **\***
    - **Host** creates an answer (e.g. `answer = await c.createAnswer()`)
    - **Host** sets it as a *local* description
    - **Host** sends it to the guest **\***
    - **Guest** sets received answer as a *remote* description **\***
    - Exchange ice candidates

3. Now peer connection is established, time to create a data channel:
    - **Host** listens for data channel creation event (e.g. `c.addEventListener('datachannel', (e: RTCDataChannelEvent) => channel = e.channel`)
    - **Guest** creates data channel (e.g. `channel = c.createDataChannel("my-app-1")`)
    - **Host** receives a data channel created by the guest and stores a ref to it (see ex. two steps above)
    - *Done! Connection established!*

4. Now use data channel objects on both ends for direct communication:
    - Listen for messages from the other end: `channel.addEventListener('message', (e: MessageEvent) => ... )`
    - Send messages: `channel.send("my message")`


**\*** - The offer and answer from one browser to another can be sent via http or web socket servers, or theoretically by writting the offer data on piece of paper and sending it via post pigeon. This is the only part where some mediator server has to be involved and it is not power demanding even for a large amount of users, thus WebRTC still can be considered as a cost effective networking solution overall in applications where all computing is possible in client browsers.

There are a few bits I left out, should there be a demand, I would do a part 2 of this guide to elaborate. 

## Summary

It is clearly not the easiest implementation to follow, but I want to emphasize that the algorithm itself is not difficult. To wrap up, here is a WebRTC connection establishement diagram (from MDN):

![WebRTC connection establishement diagram](./WebRTCDiagram.png)